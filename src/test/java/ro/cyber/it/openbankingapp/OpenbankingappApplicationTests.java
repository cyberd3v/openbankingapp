package ro.cyber.it.openbankingapp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ro.cyber.it.openbankingapp.controller.TransactionController;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class OpenbankingappApplicationTests {

	@Autowired
	private TransactionController transactionController;

	@Test
	void contextLoads() throws Exception {
		assertThat(transactionController).isNotNull();
	}

}
