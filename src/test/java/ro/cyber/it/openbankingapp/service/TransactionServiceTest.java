package ro.cyber.it.openbankingapp.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import ro.cyber.it.openbankingapp.model.Transaction;

import java.util.List;

@SpringBootTest
class TransactionServiceTest {

    @Test
    void findAllByAccountNumber() {
        int accountNumber = 111;
        int n = 1;

        TransactionService transactionService = new TransactionService();

        List<Transaction> transactions = transactionService.findAllByAccountNumber(accountNumber);

        Assertions.assertTrue(transactions.size() > n);
    }
}