package ro.cyber.it.openbankingapp.service;

import org.springframework.stereotype.Service;
import ro.cyber.it.openbankingapp.model.Transaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private static List<Transaction> transactionsList;

    public static List<Transaction> findAllByAccountNumber(int accountNumber) {
        transactionsList = new ArrayList<>();
        Transaction transaction1 = new Transaction("buy", 111, "RON", new BigDecimal("125.26"), "MegaImage", "LogoMEGAIm");
        Transaction transaction2 = new Transaction("sell", 111, "EUR",new BigDecimal("20001.59"), "PROFI", "LogoPROFI");
        Transaction transaction3 = new Transaction("borrow", 333, "GBP", new BigDecimal("300.56"), "Carrefour", "LogoCarrefour");
        Transaction transaction4 = new Transaction("sell", 333, "RON", new BigDecimal("2554.26"), "Carrefour", "LogoCarrefour");
        Transaction transaction5 = new Transaction("buy", 333, "USD", new BigDecimal("874.01"), "Carrefour", "LogoCarrefour");
        Transaction transaction6 = new Transaction("borrow", 222, "CAD", new BigDecimal("001.123"), "LIDL", "LogoLIDL");

        // added three transactions to work with
        transactionsList.add(transaction1);
        transactionsList.add(transaction2);
        transactionsList.add(transaction3);
        transactionsList.add(transaction4);
        transactionsList.add(transaction5);
        transactionsList.add(transaction6);

        List<Transaction> transactions = transactionsList
                .stream()
                .filter(l -> l.getAccountNumber() == accountNumber)
                .collect(Collectors.toList());

        if (transactions.size() > 0) {
            return transactions;
        }
        return null;
    }
}
