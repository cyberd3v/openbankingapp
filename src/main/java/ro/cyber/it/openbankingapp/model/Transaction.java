package ro.cyber.it.openbankingapp.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Transaction {
    private String type;
    private int accountNumber;
    private String currency;
    private BigDecimal amount;
    private String merchantName;
    private String merchantLogo;

    public Transaction(String type, int accountNumber, String currency, BigDecimal amount, String merchantName, String merchantLogo) {
        this.type = type;
        this.accountNumber = accountNumber;
        this.currency = currency;
        this.amount = amount;
        this.merchantName = merchantName;
        this.merchantLogo = merchantLogo;
    }
}
