package ro.cyber.it.openbankingapp.controller;

import org.springframework.web.bind.annotation.*;
import ro.cyber.it.openbankingapp.model.Transaction;
import ro.cyber.it.openbankingapp.service.TransactionService;

import java.util.List;

@RestController
@RequestMapping(path = "/transactions")
public class TransactionController {

    @GetMapping("/{accountNumber}")
    public List<Transaction> transactionById(@PathVariable("accountNumber") int accountNumber) {

        List<Transaction> allByAccountNumber = TransactionService.findAllByAccountNumber(accountNumber);

        return  allByAccountNumber;
    }

}
